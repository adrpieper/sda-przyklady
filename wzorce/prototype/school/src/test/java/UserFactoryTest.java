import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Adi on 2017-06-19.
 */
public class UserFactoryTest {

    private UserFactory userFactory = new UserFactory();

    @Test
    public void createUser() {

        User given = new User(User.Type.TEACHER, "Jan", "Kowalski", 0);
        userFactory.add("teacher", given);

        User user = userFactory.create("teacher");

        assertEquals(user, given);
        assertNotSame(user, given);
    }

    @Test
    public void returnNull() {


        User user = userFactory.create("teacher");
        assertNull(user);
    }

}