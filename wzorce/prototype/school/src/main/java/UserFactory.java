import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Adi on 2017-06-19.
 */
public class UserFactory {
    private Map<String, User> prototypes = new HashMap<>();

    public void add(String prototypeName, User prototypeUser) {
        prototypes.put(prototypeName, prototypeUser);
    }

    public Collection<String> availablePrototypes() {
        return prototypes.keySet();
    }

    public User create(String prototypeName) {
        if (prototypes.containsKey(prototypeName)) {
            return clone(prototypes.get(prototypeName));
        }
        else {
            return null;
        }
    }

    private User clone(User user) {
        try {
            return (User) user.clone();
        } catch (CloneNotSupportedException e) {
            throw new UnsupportedOperationException(e);
        }
    }
}
