/**
 * Created by Adi on 2017-06-19.
 */
public class ExampleMain {

    public static void main(String[] args) {
        UserFactory userFactory = new UserFactory();

        userFactory.add("teacher" , new User(User.Type.TEACHER, "", "", 0));
        userFactory.add("1-class" , new User(User.Type.PUPIL, "", "", 8));
        userFactory.add("2-class" , new User(User.Type.PUPIL, "", "", 9));

        User teacher = userFactory.create("teacher");
        System.out.println(teacher);
        User pupil1 = userFactory.create("1-class");
        System.out.println(pupil1);
        User pupil2 = userFactory.create("2-class");
        System.out.println(pupil2);
    }
}
